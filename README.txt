
-- SUMMARY --

This module allows you to easily and quickly integrate the GiroTrust payment method
into your website with an Ubercart online shop.

Before you can make use of GiroTrust you need to sign up with GiroSolution AG at
http://www.girosolutions.de.

-- REQUIREMENTS --

* Drupal 6 and Ubercart module 2.0.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure GiroTrust in Administer >> Store Administration >> Config Store >> Payment

-- CUSTOMIZATION --

None

-- TROUBLESHOOTING --

Not known yet

-- FAQ --

Yet to come

-- CONTACT --

Current maintainer:
* Jürgen Haas (jurgenhaas) - http://drupal.org/user/168924

This project has been sponsored by:
* PARAGON Executive Services GmbH
  Providing IT services as individual as the requirements. Find out more
  from http://www.paragon-es.de
