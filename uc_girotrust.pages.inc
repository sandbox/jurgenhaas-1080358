<?php

/**
 * @file
 * Forms for the uc_girotrust module.
 *
 */

/**
 *
 */
function uc_girotrust_payment_redirect() {
  $data = _uc_girotrust_check_hash();
  if (!$data['verified']) {
    watchdog('uc_girotrust', 'Received redirect with invalid GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
    return;
  }
  else {
    watchdog('uc_girotrust', 'Received valid redirect with GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
  }
  $order_id = $data['transactionId'];
  if (intval($_SESSION['cart_order']) != $order_id) {
    $_SESSION['cart_order'] = $order_id;
  }
  if (!($order = uc_order_load($order_id))) {
    drupal_goto('cart');
  }
  $gpCode = $data['gpCode'];
  if (_uc_girotrust_codeIsOK($gpCode)) {
    // This lets us know it's a legitimate access of the complete page.
    $_SESSION['do_complete'] = TRUE;
    drupal_goto('cart/checkout/complete');
  }
  else {
    watchdog('uc_girotrust', _uc_girotrust_getCodeText($gpCode), array(), WATCHDOG_ERROR);
    drupal_set_message(_uc_girotrust_getCodeText($gpCode), 'warning');
    drupal_goto('cart/checkout');
  }
}

/**
 *
 */
function uc_girotrust_payment_notify() {
  $data = _uc_girotrust_check_hash();
  $result = '400';
  if (!$data['verified']) {
    watchdog('uc_girotrust', t('Received notification with invalid GET data: %var'), array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
  }
  else {
    watchdog('uc_girotrust', t('Received valid notification with GET data: %var'), array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
    $order_id = $data['transactionId'];
    $order = uc_order_load($order_id);
    if ($order == FALSE) {
      watchdog('uc_girotrust', 'Notification attempted for non-existent order.', array(), WATCHDOG_ERROR);
    }
    else {
      $gpCode = $data['gpCode'];
      if (_uc_girotrust_codeIsOK($gpCode)) {
        $amount = check_plain($_GET['amount']);
        $context = array(
          'revision' => 'formatted-original',
          'location' => 'paypal-ipn',
        );
        $options = array(
          'sign' => FALSE,
        );
        $comment = t('girotrust transaction');
        uc_payment_enter($order_id, 'girotrust', $amount, $order->uid, NULL, $comment);
        uc_cart_complete_sale($order);
        uc_order_comment_save($order_id, 0, t('Payment of @amount @currency submitted through girotrust.', array('@amount' => uc_price($amount, $context, $options), '@currency' => 'EUR')), 'order', 'payment_received');
        uc_order_comment_save($order_id, 0, t('girotrust reported a payment of @amount @currency.', array('@amount' => uc_price($amount, $context, $options), '@currency' => 'EUR')));
        $result = '200';
      }
    }
  }
  print $result;
  exit;
}

// Returns the form elements for the girotrust checkout form.
function uc_girotrust_checkout_form($form_state, $order) {
  $context = array(
    'revision' => 'formatted-original',
    'location' => 'girotrust-form',
  );
  $options = array(
    'sign' => FALSE,
    'thou' => FALSE,
    'dec' => ',',
  );
  $shipping = 0;
  foreach ($order->line_items as $item) {
    if ($item['type'] == 'shipping') {
      $shipping += $item['amount'];
    }
  }
  $tax = 0;
  if (module_exists('uc_taxes')) {
    foreach (uc_taxes_calculate($order) as $tax_item) {
      $tax += $tax_item->amount;
    }
  }
  $data = array(
    'projectOwnerId' => uc_girotrust_var('project_owner_id'),
    'merchantId' => uc_girotrust_var('merchant_id'),
    'projectId' => uc_girotrust_var('project_id'),
    'transactionId' => $order->order_id,
    'amount' => uc_price($order->order_total, $context, $options),
    'vwz' => 'Order No. '. $order->order_id .'-'. $order->uid,
    'feesplit' => uc_girotrust_var('feesplit'),
    'buyer_email' => $order->primary_email,
    'buyer_address_organizationForm' => empty($order->billing_company) ? '' : t('Company'),
    'buyer_address_organizationName' => empty($order->billing_company) ? $order->billing_first_name .' '. $order->billing_last_name : $order->billing_company,
    'buyer_address_street' => $order->billing_street1,
    'buyer_address_postcode' => $order->billing_postal_code,
    'buyer_address_city' => $order->billing_city,
    'buyer_address_country' => $order->billing_country,
    'buyer_address_phone' => $order->billing_phone,
    'buyer_contact_gender' => 9,
    'buyer_contact_firstname' => $order->billing_first_name,
    'buyer_contact_lastname' => $order->billing_last_name,
    'urlRedirect' => _uc_girotrust_url_post('payment_link_redirect') .'?order_id='. $order->order_id,
    'urlNotify' => _uc_girotrust_url_post('payment_link_notify') .'?order_id='. $order->order_id .'&amount='. $order->order_total,
  );
  $hashdata = array(
    $data['projectOwnerId'],
    $data['merchantId'],
    $data['projectId'],
    $data['transactionId'],
    $data['amount'],
    $data['vwz'],
    $data['feesplit'],
    $data['buyer_email'],
    $data['buyer_address_organizationForm'],
    $data['buyer_address_organizationName'],
    $data['buyer_address_street'],
    $data['buyer_address_postcode'],
    $data['buyer_address_city'],
    $data['buyer_address_country'],
    $data['buyer_address_phone'],
    $data['buyer_contact_gender'],
    $data['buyer_contact_firstname'],
    $data['buyer_contact_lastname'],
    $data['urlRedirect'],
    $data['urlNotify'],
  );
  $data['hash'] = _uc_girotrust_get_hash($hashdata);
  $form['#action'] = _uc_girotrust_url_post('start_payment');
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => _uc_girotrust_submit(),
  );
  return $form;
}
