<?php

/**
 * @file
 * Parameter file for the uc_girotrust module.
 *
 * In this file all system parameters from GiroSolution are collected and
 * get returned to the module according to the current context.
 */

//define('UC_GIROTRUST_PSP_HOST', 'payment.girosolutions.de');
define('UC_GIROTRUST_PSP_HOST', 'jh.dev.girosolutions.de');
define('UC_GIROTRUST_WEBSITE', 'http://www.girosolutions.de');

/**
 *
 */
function _uc_girotrust_url_base() {
  return 'http://'. UC_GIROTRUST_PSP_HOST;
}

/**
 *
 */
function _uc_girotrust_url_post($type) {
  global $base_url;
  $url = _uc_girotrust_url_base();
  switch ($type) {
    case 'start_payment':
      return $url .'/payment/start';
    default:
      return $base_url .'/'. _uc_girotrust_path_post($type);
  }
}

/**
 *
 */
function _uc_girotrust_path_post($type) {
  switch ($type) {
    case 'create_project_form':
      return UC_GIROTRUST_WEBSITE;
    case 'payment_link_redirect':
      return 'cart/uc_girotrust/payment/redirect';
    case 'payment_link_notify':
      return 'cart/uc_girotrust/payment/notify';
  }
}

/**
 *
 */
function _uc_girotrust_check_hash() {
  $data = array(
    'projectOwnerId' => uc_girotrust_var('project_owner_id'),
    'merchantId' => uc_girotrust_var('merchant_id'),
    'projectId' => uc_girotrust_var('project_id'),
    'transactionId' => check_plain($_GET['order_id']),
    'gpCode' => check_plain($_GET['gpCode']),
  );
  $hashdata = array(
    $data['projectOwnerId'],
    $data['merchantId'],
    $data['projectId'],
    $data['transactionId'],
    $data['gpCode'],
  );
  $hash_check = check_plain($_GET['gpHash']);
  $hash = _uc_girotrust_get_hash($hashdata);
  if ($hash == $hash_check) {
    $data['verified'] = TRUE;
  }
  else {
    $data['verified'] = FALSE;
  }
  return $data;
}

/**
 * 
 */
function _uc_girotrust_get_hash($hashdata) {
  $hashdata_implode = implode('', $hashdata);
  return hash_hmac('md5', $hashdata_implode, uc_girotrust_var('passphrase'));
}

/**
 * 
 */
function _uc_girotrust_name() {
  return 'GiroTrust';
}

/**
 * 
 */
function _uc_girotrust_logo() {
  $logo  = drupal_get_path('module', 'uc_girotrust') .'/ressources/logo.jpg';
  $alt   = _uc_girotrust_name();
  $title = t('Payment method !name.', array('!name' => _uc_girotrust_name(),));
  return theme_image($logo, $alt, $title, array(), FALSE);
}

/**
 * 
 */
function _uc_girotrust_name_logo() {
  return _uc_girotrust_logo();
}

/**
 * 
 */
function _uc_girotrust_submit() {
  return t('Submit Order');
}

/**
 *
 */
function _uc_girotrust_description() {
  $desc = t('Mit GiroTrust wird Ihre Zahlung über einen Treuhänder abgewickelt und damit sind zu günstigen Konditionen sowohl der Käufer als auch der Verkäufer zu hundert Prozent abgesichert.');
  return _uc_girotrust_logo() .'<div>'. $desc .'</div>';
}

/**
 * Liefert zu einem girotrust Fehlercode (gpCode) die
 * deutsche Fehlerbeschreibung.
 *
 * @param integer $gpCode Fehlercode von girotrust
 * @return string Fehlerbeschreibung
 */
function _uc_girotrust_getCodeDescription($gpCode) {
  switch ($gpCode) {
    case 1900:
      return t('Fehler bei Transaktionsstart. Dei übergebenen Daten sind unbrauchbar.');
    case 1910:
      return t('Fehler/Abbruch bei Transaktionsstart. BLZ ungültig oder BLZ-Suche abgebrochen.');
    case 3100:
      return t('Benutzerseitiger Abbruch bei der Bezahlung.');
    case 4000:
      return t('Bezahlung erfolgreich.');
    case 4500:
      return t('Unbekanntes Transaktionsende. Zahlungseingang muss anhand der Kontoumsätze überprüft werden.');
    case 4900:
      return t('Bezahlung nicht erfolgreich.');
    default:
      return t('Unbekannter Fehler');
  }
}

/**
 * Liefert einen Text, der dem Benutzer nach der Bezahlung über girotrust angezeigt wird
 *
 * @param integer $gpCode Fehler Code
 * @return string Meldung für den Benutzer
 */
function _uc_girotrust_getCodeText($gpCode) {
	switch ($gpCode) {
	  case 1900:
	    return t('Fehler bei der Bezahlung über girotrust. Wir haben soeben eine E-Mail an Sie geschickt. Bitte überweisen Sie den Betrag auf das in der E-Mail angegebene Konto. Nach Geldeingang werden wir die Ware verschicken.');
	  case 1910:
	    return t('Fehler bei der Bezahlung über girotrust. Wir haben soeben eine E-Mail an Sie geschickt. Bitte überweisen Sie den Betrag auf das in der E-Mail angegebene Konto. Nach Geldeingang werden wir die Ware verschicken.');
	  case 3100:
	    return t('Sie haben die Bezahlung über girotrust abgebrochen. Wir haben soeben eine E-Mail an Sie geschickt. Bitte überweisen Sie den Betrag auf das in der E-Mail angegebene Konto. Nach Geldeingang werden wir die Ware verschicken.');
	  case 4000:
	    return t('Vielen Dank für die Bezahlung über girotrust.');
	  case 4500:
	    return t('Es ist ein unbekannter Fehler bei der Bezahlung über girotrust aufgetreten. Bitte überprüfen Sie Ihre Kontoumsätze und wenden Sie sich bei Fragen an uns.');
	  case 4900:
	    return t('Fehler bei der Bezahlung über girotrust. Wir haben soeben eine E-Mail an Sie geschickt. Bitte überweisen Sie den Betrag auf das in der E-Mail angegebene Konto. Nach Geldeingang werden wir die Ware verschicken.');
	}
	return t('Es ist ein unbekannter Fehler bei der Bezahlung über girotrust aufgetreten. Bitte überprüfen Sie Ihre Kontoumsätze und wenden Sie sich bei Fragen an uns.');
}

/**
 * Liefert, ob der angegebene Code OK bedeutet
 *
 * @param integer $gpCode Error Code
 * @return boolean
 */
function _uc_girotrust_codeIsOK($gpCode) {
	if ($gpCode == '4000') {
	  return TRUE;
  }
	return FALSE;
}

/**
 * Liefert, ob der angegebene Code ein Unbekannter ausgang ist
 *
 * @param integer $gpCode Error Code
 * @return boolean
 */
function _uc_girotrust_codeIsUnbekannt($gpCode) {
  if ($gpCode == '4500') {
	  return TRUE;
  }
	return FALSE;
}

/**
 * Liefert, ob der angegebene Code ein Fehler ist
 *
 * @param integer $gpCode Error Code
 * @return boolean
 */
function _uc_girotrust_codeIsFehler($gpCode) {
  if ($gpCode != '4000' && $gpCode != '4500') {
	  return TRUE;
  }
	return FALSE;
}
